val nums = List(1, 2, -3, 4, 5, -6, 7, 8)
val nums2 = List(123, 12352, 12434, 12414, 14, 39)

def sumNoRed(xs: List[Int]): Int = xs match {
  case Nil => 0
  case y :: ys => y + sumNoRed(ys)
}

def prodNoRed(xs: List[Int]): Int = xs match {
  case Nil => 1
  case y :: ys => y * prodNoRed(ys)
}

sumNoRed(nums)
prodNoRed(nums)


//  abstract class ListRedFold[T] {
//    def reduceLeft(op: (T, T) => T): T = this match {
//      case Nil => throw new Error("Nil.reduceLeft")
//      case x :: xs => (xs foldLeft x) (op)
//    }
//
//    def foldLeft[U](z: U)(op: (U, T) => U): U = this match {
//      case Nil => z
//      case x :: xs => (xs foldLeft op(z, x)) (op)
//    }
//  }


def sumReduce(xs: List[Int]) = (0 :: xs) reduceLeft ((x, y) => x + y)

def prodReduce(xs: List[Int]) = (1 :: xs) reduceLeft (_ * _)

sumReduce(nums)
prodReduce(nums)

def sumFoldLeft(xs: List[Int]) = (xs foldLeft 0) (_ + _)
def prodFoldLeft(xs: List[Int]) = (xs foldLeft 1) (_ * _)
def sumFoldRight(xs: List[Int]) = (xs foldRight 0) (_ + _)
def prodFoldRight(xs: List[Int]) = (xs foldRight 1) (_ * _)

sumFoldLeft(nums)
prodFoldLeft(nums)
sumFoldRight(nums)
prodFoldRight(nums)

def concat[T](xs: List[T], ys: List[T]): List[T] = xs.foldRight(ys)(_ :: _)
concat(nums, nums2)

// no :: method in element (type of element is T)
//def concatLeft[T](xs: List[T], ys: List[T]): List[T] = xs.foldLeft(ys)(_ :: _)
//concatLeft(nums, nums2)