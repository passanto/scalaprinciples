def msort[T](xs: List[T])(lt: (T, T) => Boolean): List[T] = {
  val n = xs.length / 2
  if (n == 0) xs
  else {
    def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
      case (Nil, ys) => ys
      case (xs, Nil) => xs
      case (x :: xs1, y :: ys1) => {
        if (lt(x, y)) x :: merge(xs1, ys)
        else y :: merge(xs, ys1)
      }
    }

    val (fst, scnd) = xs.splitAt(n)
    merge(msort(fst)(lt), msort(scnd)(lt))
  }
}

val numsT = List(1, -2, 3, -8)
msort(numsT)((x, y) => x < y)

val fruit = List("apple", "orange", "pear", "banana")
msort(fruit)((x, y) => x.compareTo(y) < 0)



//-------------------with ordering as function parameter
def msortWO[T](xs: List[T])(ord: Ordering[T]): List[T] = {
  val n = xs.length / 2
  if (n == 0) xs
  else {
    def mergeWO(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
      case (Nil, ys) => ys
      case (xs, Nil) => xs
      case (x :: xs1, y :: ys1) => {
        if (ord.lt(x, y)) x :: mergeWO(xs1, ys)
        else y :: mergeWO(xs, ys1)
      }
    }

    val (fst, scnd) = xs.splitAt(n)
    mergeWO(msortWO(fst)(ord), msortWO(scnd)(ord))
  }
}

msortWO(numsT)(Ordering.Int)
msortWO(fruit)(Ordering.String)





//-------------------with implicit ordering
def msortWiO[T](xs: List[T])(implicit ord: Ordering[T]): List[T] = {
  val n = xs.length / 2
  if (n == 0) xs
  else {
    def mergeWiO(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
      case (Nil, ys) => ys
      case (xs, Nil) => xs
      case (x :: xs1, y :: ys1) => {
        if (ord.lt(x, y)) x :: mergeWiO(xs1, ys)
        else y :: mergeWiO(xs, ys1)
      }
    }

    val (fst, scnd) = xs.splitAt(n)
    mergeWiO(msortWiO(fst), msortWiO(scnd))
  }
}

msortWiO(numsT)
msortWiO(fruit)