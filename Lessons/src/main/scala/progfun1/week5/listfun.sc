val fruit = List("apple", "orange", "pear")
val nums = List(1, 2, -3, 4, 5, -6, 7, 8)


def squareListNoMap(xs: List[Int]): List[Int] = xs match {
  case Nil => xs
  case y :: ys => (y * y) :: squareListNoMap(ys)
}
squareListNoMap(nums)

def squareList(xs: List[Int]): List[Int] = {
  xs map (x => x * x)
}
squareList(nums)


def posElemNoMap(xs: List[Int]): List[Int] = xs match {
  case Nil => xs
  case y :: ys => if (y > 0) y :: posElemNoMap(ys) else posElemNoMap(ys)
}
posElemNoMap(nums)

def posElem(xs: List[Int]): List[Int] = {
  xs filter (x => (x > 0))
}
posElem(nums)

nums filterNot (x => x > 0)
nums partition (x => x > 0)

nums takeWhile (x => x > 0)
nums dropWhile (x => x > 0)

nums span (x => x > 0)

val data = List("a", "a", "b", "c", "c", "a")

def pack[T](xs: List[T]): List[List[T]] = xs match {
  case Nil => Nil
  case x :: xs1 =>
    val (first, rest) = xs span (y => y == x)
    first :: pack(rest)
}


def encode[T](xs: List[T]): List[(T, Int)] = pack(xs) map
  (ys => (ys.head, ys.length))

encode(data)