val fruit = List("apple", "orange", "pear")
val nums = List(1, 2, 3)
val diag = List(List(1, 0, 0), List(0, 1, 0), List(0, 0, 1))
val empty = List()


def init[T](xs: List[T]): List[T] = xs match {
  case List() => throw new Error("init of empty list")
  case List(x) => List(x)
  case y :: ys => y /*List(y) ++ */ :: init(ys)
}

def concat[T](xs: List[T], ys: List[T]): List[T] = xs match {
  case List() => ys
  case z :: zs => z :: concat(zs, ys)
}

// n^2 complexity... can be done better?
def reverse[T](xs: List[T]): List[T] = xs match {
  case List() => xs
  case y :: ys => reverse(ys) ::: List(y)
}

def removeAt(n: Int, xs: List[Int]): List[Int] =
  (xs take n) ::: (xs drop n + 1)


concat((concat(concat(fruit, nums), diag)), empty)
reverse(fruit)
removeAt(1, nums)
// exception -> empty.head


def isort(xs: List[Int]): List[Int] = xs match {
  case List() => List()
  case y :: ys => insert(y, isort(ys))
}

def insert(x: Int, xs: List[Int]): List[Int] = xs match {
  case List() => List(x)
  case y :: ys =>
    if (x <= y) x :: xs
    else y :: insert(x, ys)
}

isort(List(2, 5, 8, 3, 5, 7))

def last[T](xs: List[T]): T = xs match {
  case List() => throw new Error("Last of empty")
  case List(x) => x
  case y :: ys => last(ys)
}


init(List(1, 2, 3, 4, 5, 6, 7, 8, 9))
concat(List(1, 2, 3, 4), List(5, 6, 7, 8, 9))
reverse(List(1, 2, 3, 4, 5, 6, 7, 8, 9))
