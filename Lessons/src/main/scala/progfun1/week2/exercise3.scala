package week2

/*
Lecture 2/3

EXAMPLES

A number x is called a fixed point of a function f if:
  f(x) = x

For some functions f we can locate the fixed points by starting with
an initial estimate and then by applying f in a repetitive way.

  x, f(x), f(f(x)), f(f(f(x))), ...

until the value does not vary anymore (or the change is sufficiently small).

*/

import math.abs

object exercise3 {

  // finding a fixed point
  // ----------------------------------------------
  val tolerance = 0.0000001

  def isCloseEnough(x: Double, y: Double): Boolean = {
    // println("isCloseEnough:" + x + " - " + y)
    abs((x - y) / x) < tolerance
  }

  def fixedPoint(f: Double => Double)(firstGuess: Double): Double = {
    def iterate(guess: Double): Double = {
      // println("guess:" + guess)
      val next = f(guess)
      // println("next:" + next)
      if (isCloseEnough(guess, next)) next
      else iterate(next)
    }

    iterate(firstGuess)
  }

  def main(args: Array[String]): Unit = {

    //graphical function
    println(fixedPoint((x => 1 + x / 2))(1))


    // sqrt function
    // ----------------------------------------------
    /*
    sqrt(x) = y => y*y=x
    sqrt(x) = y => y=x/y

    hence
    sqrt(x) is a fixed point of (y=>x/y)

    */
    def sqrt(x: Double): Double = fixedPoint(y => x / y)(1.0)
    // commented since doesn't converge
    // sqrt(9)

    /*
    this "oscillates" too much:
      1,9,1,9,1,9,....

    to prevent the estimation to vary too much it can be done with
    averaging successive values of the original sequence:
    */
    def sqrtAvg(x: Double): Double = fixedPoint(y => (y + x / y) / 2)(1.0)

    println(sqrtAvg(2))
    println(sqrtAvg(9))


    /*
    The technique of "stabilizing by averaging" can be abstracted into a general function that
    takes a function and a value and returns the "average" of
    the value and the result of the function applied to it
     */
    def averageDump(f: Double => Double)(a: Double): Double = (a + f(a)) / 2

    /*
    The SQRT function can than be written as:
     */
    def sqrtAvgDump(x: Double) = fixedPoint(averageDump(y => x / y))(1.0)

    println("Generalized SQRT with averageDump: " + sqrtAvgDump(9))

  }
}
