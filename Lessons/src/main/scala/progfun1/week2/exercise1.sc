/*
Lecture 2/1

HIGH ORDER FUNCTIONS
*/

import scala.annotation.tailrec

// auxs functions
def id(x: Int): Int = x
def cube(x: Int): Int = x * x * x
def fact(x: Int): Int = if (x == 0) 1 else x * fact(x - 1)

val fact3 = fact(3)
val fact4 = fact(4)
val fact5 = fact(5)

// sum function
def sum(f: Int => Int, a: Int, b: Int): Int = {
  if (a > b) 0
  else f(a) + sum(f, a + 1, b)
}

// derived functions
def sumInts(a: Int, b: Int): Int = sum(id, a, b)
def sumCubes(a: Int, b: Int): Int = sum(cube, a, b)
def sumFacts(a: Int, b: Int): Int = sum(fact, a, b)

sumInts(3, 5)
sumCubes(3, 5)
val fact35 = sumFacts(3, 5)

//----------------------------------------------------------

// with anonymous functions
def sumIntsAnon(a: Int, b: Int): Int = sum(x => x, a, b)
def sumCubesAnon(a: Int, b: Int): Int = sum(x => x * x * x, a, b)
def sumFactsAnon(a: Int, b: Int): Int = sum(x => {
  if (x == 0) 1 else x * fact(x - 1)
}, a, b)


sumIntsAnon(3, 5)
sumCubesAnon(3, 5)
sumFactsAnon(3, 5)


//----------------------------------------------------------
// tail recursion
def tailRecSum(f: Int => Int)(a: Int, b: Int): Int = {
  @tailrec
  def loop(a: Int, acc: Int): Int = {
    if (a > b) acc
    else loop(a + 1, acc + f(a))
  }

  loop(a, 0)
}

def sumIntsRec(a: Int, b: Int): Int = tailRecSum(id)(a, b)
def sumCubesRec(a: Int, b: Int): Int = tailRecSum(cube)(a, b)
def sumFactsRec(a: Int, b: Int): Int = tailRecSum(fact)(a, b)

sumIntsRec(3, 5)
sumCubesRec(3, 5)
sumFactsRec(3, 5)
