/*
Lecture 2/2

CURRYING
*/

// auxs functions
def id(x: Int): Int = x
def cube(x: Int): Int = x * x * x
def fact(x: Int): Int = if (x == 0) 1 else x * fact(x - 1)
def prod(x: Int, y: Int) = x * y

def sum(f: Int => Int): ((Int, Int) => Int) = {
  def sumF(a: Int, b: Int): Int = {
    if (a > b) 0
    else f(a) + sumF(a + 1, b)
  }

  sumF
}

def sumInts: (Int, Int) => Int = sum(id)
def sumCubes: (Int, Int) => Int = sum(cube)
def sumFacts: (Int, Int) => Int = sum(fact)

sumInts(3, 5)
sumCubes(3, 5)
sumFacts(3, 5)

// as sumInt
sum(id)(3, 5)
// as sumCubes
sum(cube)(3, 5)
// as sumFacts
sum(fact)(3, 5)

// unnecessary parenthesis
// left for further comprehension
//
// as sumInt
(sum(id)) (3, 5)
// as sumCubes
(sum(cube)) (3, 5)
// as sumFacts
(sum(fact)) (3, 5)


// --------------------------------------------------------
// shorter versions with special Scala syntax
//
// sumShorter type is:
// (Int=>Int) => ((Int, Int) => Int)
// a function that takes a function as a parameter
// and returns a function which takes 2 Int and return an Int
def sumShorter(f: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) 0
  else f(a) + sumShorter(f)(a + 1, b)
}

def sumIntsShorter: (Int, Int) => Int = sumShorter(id)
def sumCubesShorter: (Int, Int) => Int = sumShorter(cube)
def sumFactsShorter: (Int, Int) => Int = sumShorter(fact)

sumIntsShorter(3, 5)
sumCubesShorter(3, 5)
sumFactsShorter(3, 5)

sumShorter(id)(3, 5)
sumShorter(cube)(3, 5)
sumShorter(fact)(3, 5)

(sumShorter(id)) (3, 5)
(sumShorter(cube)) (3, 5)
(sumShorter(fact)) (3, 5)


//
// EXERCISES:
//
// 1:
// Write a product function that calculates the product of the values of
// a function for the points on a given interval.
def product(f: Int => Int)(a: Int, b: Int): Long = {
  if (a > b) 1
  else f(a) * product(f)(a + 1, b)
}

product(x => x ^ 2)(3, 7)
product(id)(3, 7)
product(cube)(3, 7)
product(fact)(3, 7)

// 2:
// Write factorial in terms of product
def factorialWithProduct(x: Int): Long = {
  product(id)(1, x)
}
factorialWithProduct(3)
factorialWithProduct(5)


//
// Can you write a more general function, which generalizes both sum
// and product?
//
// combine will be product or sum (or another operation)
def myMapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int = {
  if (a > b) zero
  else combine(f(a), myMapReduce(f, combine, zero)(a + 1, b))
}

myMapReduce(id, prod, 1)(3, 7)
def myMapReduceProduct(a: Int, b: Int): Long = {
  myMapReduce(id, prod, 1)(a, b)
}
myMapReduceProduct(3, 7)