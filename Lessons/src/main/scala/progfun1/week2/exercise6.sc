/*
Lecture 2/7

EVALUATION AND OPERATORS

The precedence of an operator is determined by its first character.

The following table lists the characters in INCREASING order of priority
precedence:

(all letters)
|
^
&
< >
= !
:
+ -
* / %
(all other special characters)


EXERCISE:
which is the correct parenthesization of:
a + b ^? c ?^ d less a ==> b | c

1st: ?^
a + b ^? (c ?^ d) less a ==> b | c

2dn: +
(a + b) ^? (c ?^ d) less a ==> b | c

3th: ==>
(a + b) ^? (c ?^ d) less (a ==> b) | c

4th: ==>
(a + b) ^? (c ?^ d) less (a ==> b) | c

5th: ^
((a + b) ^? (c ?^ d)) less (a ==> b) | c

6th: |
((a + b) ^? (c ?^ d)) less ((a ==> b) | c)

7th: less
(((a + b) ^? (c ?^ d)) less ((a ==> b) | c))

*/

class Rational(x: Int, y: Int) {
  require(y != 0, "Denominator != 0...")

  def this(x: Int) = this(x, 1)

  private def gcd(a: Int, b: Int): Int = {
    if (b == 0) a else gcd(b, a % b)
  }


  def numer: Int = x

  def denom: Int = y

  // less
  def <(that: Rational): Boolean =
    numer * that.denom < that.numer * denom

  // maximum
  def max(that: Rational): Rational =
    if (this < that) that else this

  // sum
  def +(that: Rational): Rational = {
    new Rational(numer * that.denom + that.numer * denom, //
      denom * that.denom)
  }

  // neg
  def unary_- : Rational = new Rational(-numer, denom)

  // subtract
  def -(that: Rational): Rational = {
    this + -that
  }

  // this can lead to overflow...
  override
  def toString: String = {
    val g = gcd(x, y)
    numer / g + "/" + denom / g
  }
} // Rational class

val x = new Rational(1, 3)
val y = new Rational(5, 7)
val z = new Rational(3, 2)

x < y

x max y
x max y max z

x + y
x + y + z

x - y
x + -y

x - y - z
x + -y + -z

-x