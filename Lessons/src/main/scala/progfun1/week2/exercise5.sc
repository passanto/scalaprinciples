import scala.annotation.tailrec

/*
Lecture 2/5-6

FUNCTIONS AND DATA
*/


class Rational(x: Int, y: Int) {
  /*
  primary constructor takes the parameters
  and executes the body of the class

  custom constructors are available:
   */
  def this(x: Int) = this(x, 1)


  /*
  Lecture 2/6 //////////////////////////////
   */
  //
  // require enforces a preconditions to the caller
  require(y != 0, "Denominator has to be different than 0")
  //
  // assert checks the functionality
  assert(y != 0, "Denominator has to be different than 0")

  @tailrec
  private def gcd(a: Int, b: Int): Int = {
    if (b == 0) a else gcd(b, a % b)
  }

  private val g = gcd(x, y)

  /*def numer: Int = x / g

  def denom: Int = y / g*/

  /*
  numer and denom can be val
  so they are calculated only once
   */
  val numer: Int = x / g

  val denom: Int = y / g

  def less(that: Rational): Boolean = {
    numer * that.denom < that.numer * denom
  }

  def max(that: Rational): Rational = {
    if (this less that) that
    else this
  }

  /*
 Lecture 2/6 //////////////////////////////
 */
  def add(that: Rational): Rational = {
    new Rational(
      numer * that.denom + that.numer * denom,
      denom * that.denom
    )
  }

  def subtract(that: Rational): Rational = {
    add(that.neg)
  }

  def neg: Rational = {
    new Rational(
      -1 * numer,
      denom
    )
  }

  override
  def toString: String = numer + "/" + denom
} // RATIONAL class

// external functions
//
def addRational(r: Rational, s: Rational): Rational = {
  new Rational(
    r.numer * s.denom + s.numer * r.denom,
    r.denom * s.denom
  )
}

def negRational(r: Rational): Rational = {
  new Rational(
    -1 * r.numer,
    r.denom
  )
}

def subtractRational(r: Rational, s: Rational): Rational = {
  r.add(negRational(s))
}




val x = new Rational(1, 3)
val y = new Rational(5, 7)
val z = new Rational(3, 2)

x.numer
x.denom

x.add(y)
addRational(x, y)

x.neg
negRational(x)

x.subtract(y)
subtractRational(x, y)
x.subtract(y).subtract(z)

y.add(y)
addRational(y, y)

x less y

x max y

new Rational(2)


//
// exercise:
//
// Modify the Rational class so that rational numbers are kept
// unsimplified internally, but the simplification is applied when
// numbers are converted to strings.

class RationalUnsimplified(x: Int, y: Int) {
  def numer: Int = x

  def denom: Int = y

  @tailrec
  private def gcd(a: Int, b: Int): Int = {
    if (b == 0) a else gcd(b, a % b)
  }

  private val g = gcd(numer, denom)

  override
  def toString: String = numer / g + "/" + denom / g
}

// Rational is better since simplifications
// are better if executed as soon as possible

// exercise-------------------------------------


// fails the checks/validation
val strange = new Rational(1, 0)
strange add strange