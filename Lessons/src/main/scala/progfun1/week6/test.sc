val xs = Array(1, 2, 3, 44)
xs map (x => x * 2)

val s = "Hello World!"
s filter (c => c.isUpper)

val r: Range = 1 until 10
val r1: Range = 1 to 10

1 to 10 by 3
6 to 2 by -2

s exists (c => c.isUpper)
s forall (c => c.isUpper)

val pairs = List(1, 2, 3) zip s
pairs.unzip
pairs.unzip._1

s flatMap (c => List(c, '.'))

xs.sum
xs.max

val M = 5;
val N = 4;
(1 to N) map (y => List(y))
(1 to N) flatMap (y => List(y))
(1 to M) map (x => (1 to N) map (y => List(x, y)))
(1 to M) flatMap (x => (1 to N) map (y => List(x, y)))

def scalarProduct(xs: Vector[Double], ys: Vector[Double]): Double = {
  (xs zip ys).map(xy => xy._1 * xy._2).sum
}

def scalarProductPatMat(xs: Vector[Double], ys: Vector[Double]): Double = {
  (xs zip ys).map { case (x, y) => x * y }.sum
}


val v1 = Vector[Double](1, 2, 3, 4, 5, 6)
val v2 = Vector[Double](7, 8, 3, 2, 5, 1, 2, 5)

v1 zip v2

scalarProduct(v1, v2)
scalarProductPatMat(v1, v2)




def isPrime(n: Int): Boolean = {
  (2 until n).forall(x => (n % x != 0))
}

isPrime(7)
isPrime(6)
isPrime(9)