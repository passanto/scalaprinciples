def isPrime(n: Int): Boolean = {
  (2 until n).forall(x => (n % x != 0))
}

val n = 7
(1 until n).mkString(", ")

//next tow lead to the same result
((1 until n) map (i => (1 until i) map (j => (i, j)))).flatten
(1 until n) flatMap (i => (1 until i) map (j => (i, j)))

(1 until n) flatMap (i => (1 until i) map (j => (i, j))) filter (pair => isPrime(pair._1 + pair._2))

// for expression
for {
  i <- 1 until n
  j <- 1 until i
  if isPrime(i + j)
} yield (i, j)


def scalarProduct(xs: Vector[Double], ys: Vector[Double]): Double = {
  (for {
    (x, y) <- xs zip ys
  } yield x * y)
    .sum
}

val v1 = Vector[Double](1, 2, 3, 4, 5, 6)
val v2 = Vector[Double](7, 8, 3, 2, 5, 1, 2, 5)

v1 zip v2

scalarProduct(v1, v2)



// more flatMap s

val fruits = Seq("apple", "banana", "orange")
fruits.map(_.toUpperCase)
fruits.flatMap(_.toUpperCase)
