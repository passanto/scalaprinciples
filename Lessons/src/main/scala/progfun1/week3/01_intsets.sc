import week3.classes.IntSet.NonEmpty
import week3.classes.IntSet.Empty

val t1 = new NonEmpty(3, Empty, Empty)
val t2 = t1 incl 4


// Polymorphism
// lecture 3/1
//
abstract class Base {
  def foo = 1

  def bar: Int
}

class Sub extends Base {
  override def foo = 2

  def bar = 3
}

new Base {
  override def bar = 3
}.foo

(new Sub).foo
(new Sub).bar