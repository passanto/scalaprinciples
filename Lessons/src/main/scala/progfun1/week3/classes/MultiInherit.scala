package week3.classes

class MultiInherit(x: Int, y: Int) extends Rational(x: Int, y: Int)
  with Trait1 with Trait2 {

  // from Trait1
  override def method1: Unit = super.method1

  // from Trait2
  override def method2: Unit = super.method2

  // from Rational
  override
  def toString: String = {
    val g = super.gcd(x, y)
    numer / g + "/" + denom / g
  }

  def error(msg: String) = throw new Error(msg)

}
