package week3.classes

class Rational(x: Int, y: Int) {
  require(y != 0, "Denominator != 0...")

  def this(x: Int) = this(x, 1)

  protected def gcd(a: Int, b: Int): Int = {
    if (b == 0) a else gcd(b, a % b)
  }

  /*
    will be evaluated when referenced

    def numer = x

    def denom = y
  */

  val numer = x

  val denom = y

  def <(that: Rational) =
    numer * that.denom < that.numer * denom

  def max(that: Rational) =
    if (this < that) that else this


  def +(that: Rational): Rational = {
    new Rational(numer * that.denom + that.numer * denom, //
      denom * that.denom)
  }

  def unary_- : Rational = new Rational(-numer, denom)


  def -(that: Rational) = {
    this + -that
  }

  // this can lead to overflow...
  override
  def toString: String = {
    val g = gcd(x, y)
    numer / g + "/" + denom / g
  }
}