package week3.classes

import java.util.NoSuchElementException

trait List[T] {

  def isEmpty: Boolean

  def head: T

  def tail: List[T]

}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  def isEmpty = false

  // "val head" and "val tail" are methods
  // head and tail implementations are the parameters themselves
}

class Nil[T] extends List[T] {
  def isEmpty = true

  def head: Nothing = throw new NoSuchElementException("Nil.head")

  def tail: Nothing = throw new NoSuchElementException("Nil.tail")
}


