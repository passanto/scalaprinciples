package week3.classes

// traits cannot have constructor parameter
trait Trait1 /*(x: Int, y: Int)*/ {

  def val1 = "hello"

  def method1 = {
    println(val1)
  }


}
