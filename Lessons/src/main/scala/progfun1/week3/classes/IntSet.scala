package week3.classes

object IntSet {

  abstract class IntSet {
    def zero = 0

    def incl(x: Int): IntSet

    def contains(x: Int): Boolean

    def union(other: IntSet): IntSet

  } // abstract class IntSet


  // instead of class...
  // class Empty extends IntSet {
  //
  // "object" defines a Singleton instance
  // it gets created at its first reference
  object Empty extends IntSet {
    // redefines a NON-ABSTRACT definition
    override def zero: Int = 1

    def contains(x: Int): Boolean = false

    def incl(x: Int) = new NonEmpty(x, Empty, Empty)

    def union(other: IntSet): IntSet = other

    override def toString: String = "_"

  } // class Empty

  class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {
    def incl(x: Int): NonEmpty =
      if (x < elem) new NonEmpty(elem, left incl x, right)
      else if (x > elem) new NonEmpty(elem, left, right incl x)
      else this

    def contains(x: Int): Boolean =
      if (x < elem) left contains x
      else if (x > elem) right contains x
      else true

    def union(other: IntSet): IntSet =
      ((left union right) union other) incl elem

    override def toString: String = "{" + left + elem + right + "}"

  } // class NonEmpty

  def main(args: Array[String]): Unit = {
    val t1 = new NonEmpty(3, Empty, Empty)
    val t2 = t1 incl 4
    println("T1: " + t1)
    println("T2: " + t2)
    println("T1 union T2: " + t1.union(t2))
    println("T2 union T1: " + t2.union(t1))
  }


}
