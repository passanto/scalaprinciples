// import a class
//
// import week3.classes.Rational

// import a set of classes
//
// import week3.classes.{IntSet, Rational}

// import all classes
//
// import week3.classes._

import week3.classes.Rational

val r = new Rational(1, 2)



//
// TRAITS
//
// Scala (as Java) is single inheritance language
//
// Traits are like abstract classes
// and can be defined simlarly
//
// Traits resemble interfaces in Java but
//    -they can contains fields and concrete methods
//    -cannot have (val) parameters, only abstract definitions (def)
//
// trait Planar {
//    def height: Int
//    def width: Int
//    def surface = height * width  // this is an implemented method
// }
//
// Classes, objects and traits can inherit from at most one class
// but arbitrary many traits:
//
// class Square extends Shape with Planar with Movable
//
//
//
// .













val x = null
val y: String = x

// null is subtype of all AnyRef (sequences, strings...)
// Int is a subtype of AnyVal (double, float, char, boolean...)
//
// hence type mismatch:
//
// val z: Int = null
//
//

// Int (1) is a subtype of AnyVal
// Boolean (false) is a subtype of AnyVal
// next is AnyVal
if (true) 1 else false

// error
// error
throw new Error("error")
