// Immutable linked lists:
// lecture 3/3
//
// Made up of two building blocks:
// Nil:   the empty list
// Cons:  cell containing an element and the rest of the list

// examples:
//
List(1, 2, 3)
List(List(true, false), List(3))


// [T] is the type parameter of the list's elements
// ( _ is used for differentiating from the classes imported)
//
trait List_[T]

class Cons_[T](val head: T, val tail: List_[T]) extends List_[T]

class Nil_[T] extends List_[T]

// functions can also have type parameters:
// (generics)
//
def singleton[T](elem: T) = new Cons_[T](elem, new Nil_[T])

singleton[Int](1)
// the compiler can deduce the type from the value of the argument:
singleton(1)
singleton[Boolean](elem = true)


// write a method that returns the nth element of a list
//
import week3.classes.{Cons, List, Nil}
import scala.annotation.tailrec

@tailrec
def nth[T](n: Int, list: List[T]): T = {
  if (list.isEmpty) throw new IndexOutOfBoundsException
  else if (n == 0) list.head
  else nth(n - 1, list.tail)
}

val list = new Cons(1, new Cons(2, new Cons(3, new Nil)))

nth(1, list)
nth(3, list)
nth(-1, list)


