// lists:
// lecture 4/7
//
// Lists differ from array since they are:
//
//  -immutable
//  -recursive
//
//
// All lists are constructed from:
//  -empty list Nil
//  -the construction operator (x :: xs)  // xs can be empty
//
//  val nums = 1 :: 2 :: 3 :: 4 :: Nil
// is the same as
//  Nil.::(4).::(3).::(2).::(1)
//
//  All operations on lists can be expressed as combination of:
//    -head     (first element)
//    -tail     (the list without the head)
//    -isEmpty
//
//
// .


// N*N complexity
def isort(xs: List[Int]): List[Int] = {

  // worst case (n > all xs elements) complexity:
  // N recursive call
  def insert(x: Int, xs: List[Int]): List[Int] = xs match {
    case List() => List(x)
    case y :: ys => if (x <= y) x :: xs else y :: insert(x, ys)
  }

  // N insert calls for each element
  xs match {
    case List() => List()
    case y :: ys => insert(y, isort(ys))
  }
}


isort(List(7, 3, 9, 2))
isort(List())
isort(List(7, 3))
