import week3.classes.IntSet.{Empty, IntSet, NonEmpty}
// Empty -> IntSet        // extends
// NonEmpty -> IntSet


// covariance:
// lecture 4/4
//


//
// C[A] <: C[B]                 // C is covariant
// C[A] >: C[B]                 // C is contravariant
// neither C[A] nor C[B] is     // C is nonvariant
//    a subtype of the other
//
// in Scala we can declare the variance of a type:
//
//  class C[+A] { ... }        // C is covariant
//  class C[-A] { ... }        // C is contravariant
//  class C[A] { ... }         // C is nonvariant
//
//
// Say:
// type A = IntSet => NonEmpty
// type B = NonEmpty => IntSet
//
// based on the Liskov principle, which of the following is true?
//  A <: B    <- B is a subtype of A
//  B <: A
//  A and B are unrelated
//
//  General rule:
//  if
//    A2 <: A1 and B1 <: B2
//  then
//    A1 => B1 <: A2 => B2
//
//  As a result, functions are
//    CONTRAVARIANT in their argument (parameter) types
//    VARIANT in their return type
//
//  Function1 can be revised as:
trait Function1[-T, +U] {
  def apply(x: T): U
}

//  .







