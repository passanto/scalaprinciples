package week4.classes

trait Expr1 {

  def isNumber: Boolean

  def isSum: Boolean

  def numValue: Int

  def leftOp: Expr1

  def rightOp: Expr1

}

class Number1(n: Int) extends Expr1 {
  def isNumber: Boolean = true

  def isSum: Boolean = false

  def numValue: Int = n

  def leftOp: Expr1 = throw new Error("Number.leftOp")

  def rightOp: Expr1 = throw new Error("Number.rightOp")
}

class Sum1(e1: Expr1, e2: Expr1) extends Expr1 {
  def isNumber: Boolean = false

  def isSum: Boolean = true

  def numValue: Int = throw new Error("Sum.numValue")

  def leftOp: Expr1 = e1

  def rightOp: Expr1 = e2
}


