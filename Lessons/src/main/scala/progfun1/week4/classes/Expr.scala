package week4.classes

// Case classes are regular classes which export their constructor parameters
// and which provide a recursive decomposition mechanism via pattern matching
//
// to instantiate a Case class the "new" keyword isn't required since
// case classes define the apply method as the AllArgsConstructor
// .
trait Expr {
  def eval: Int = this match {
    case Number(n) => n
    case Sum(e1, e2) => e1.eval + e2.eval
  }
}

case class Number(n: Int) extends Expr

case class Sum(e1: Expr, e2: Expr) extends Expr

case class Prod(e1: Expr, e2: Expr) extends Expr

// balanced show case class
case class Var(v: String) extends Expr

case class ProdOfExpr(e1: Expr, e2: Expr) extends Expr
