import week4._

object exprs {
  def show(e: Expr): String = e match {
    case Number(x) => x.toString

    case Sum(l, r) => show(l) + "+" + show(r)

    // case Prod...?
    case Prod(Sum(sl, sr), r) => "(" + show(sl) + " + " + show(sr) + ")" + "*" + show(r)
    case Prod(l, r) => show(l) + "*" + show(r)
    case Var(name) => name

  }

  show(Sum(Number(2), Number(4)))
  show(Sum(Prod(Number(2), Var("x")), Var("y"))) //> res1: String = 2*x + y
  show(Prod(Sum(Number(2), Var("x")), Var("y"))) //> res2: String = (2 + x)*y

}