import week4.{Cons, Nil, List}

def nTh[T](n: Int, list: List[T]): T = {
  if (list.isEmpty) throw new IndexOutOfBoundsException("empty list")
  else if (n == 0) list.head
  else nTh(n - 1, list.tail)
}

val l = new Cons(1, //
  new Cons(2, //
    new Cons(3,  Nil)))

nTh(0, l)
nTh(1, l)
nTh(2, l)
// error:
// nTh(-4, l)


// functions as objects:
val list1 = List[Int](1, 2)
val list2 = List[Int](3)
val list3 = List[Int]()


