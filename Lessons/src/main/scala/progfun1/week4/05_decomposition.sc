import week4.classes.{Expr1, Sum1, Number1}

// decomposition:
// lecture 4/5
//


// OO decomposition isn't always advisable



def eval(e: Expr1): Int = {
  if (e.isNumber) e.numValue
  else if (e.isSum) eval(e.leftOp) + eval(e.rightOp)
  else throw new Error("Unknown expression " + e)
}


def evalWithCast(e: Expr1): Int =
  if (e.isInstanceOf[Number1])
    e.asInstanceOf[Number1].numValue
  else if (e.isInstanceOf[Sum1])
    eval(e.asInstanceOf[Sum1].leftOp) +
      eval(e.asInstanceOf[Sum1].rightOp)
  else throw new Error("Unknown expression" + e)