import week3.classes.IntSet.{Empty, IntSet, NonEmpty}
// Empty -> IntSet        // extends
// NonEmpty -> IntSet


// subtyping and generics:
// lecture 4/3
//


//
// S <: T
// S is a subtype of T
//
// there can be combination of "lower bounds":
// S <: T >: R
// S is upper bounded by T and lower bounded by R
//
// next function accepts all subtypes of IntSet (included)
def assertAllPo[S <: IntSet](r: S): S = ???


// given the following:
//
// NonEmpty <: IntSet
//
// is next also valid?
// List[NonEmpty] <: List[IntSet]
//
//  COVARIANT types are the ones for which the above holds
//
// NO:
// In SCALA arrays are Not-Covariant!
val a: Array[NonEmpty] = Array(new NonEmpty(1, Empty, Empty))
val b: Array[IntSet] = a // <--- non-covariant
b(0) = Empty
val s: NonEmpty = a(0)
//
//
// In SCALA:
// Mutable collections (as Arrays) are NOT-COVARIANT
// Immutable collections (as Lists) are COVARIANT
//    (if some further conditions are met)
//
//
//
// LISKOV Substitution Principle:
//  if
//    A <: B
//  then everything one can to do with a value of
//  type B one should also be able to do with a value of type A
//
//


