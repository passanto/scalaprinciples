import week4.classes.{Expr, Number, Prod, Sum, Var, ProdOfExpr}

// pattern matching:
// lecture 4/6
//

def eval(e: Expr): Int = e match {
  case Number(n) => n
  case Sum(e1, e2) => eval(e1) + eval(e2)
}

eval(Sum(Number(1), Number(2)))

def show(e: Expr): String = e match {
  case Number(x) => x.toString
  case Sum(e1, e2) => show(e1) + " + " + show(e2)
  case Prod(e1, e2) => e1 match {
    case Sum(_, _) => "(" + show(e1) + ")" + " * " + show(e2)
  }
}

show(Sum(Number(1), Number(2)))


// prints necessary parenthesis
def showBalanced(e: Expr): String = e match {
  case Var(v) => v
  case Number(x) => x.toString
  case Sum(e1, e2) => showBalanced(e1) + " + " + showBalanced(e2)
  case ProdOfExpr(left, right) => showBalanced(left) + " * " + showBalanced(right)
  case ProdOfExpr(Sum(e1, e2), r) =>
    "(" + showBalanced(Sum(e1, e2)) + ")" + " * " + showBalanced(r)
  case ProdOfExpr(l, Sum(e1, e2)) =>
    showBalanced(l) + " * " + "(" + showBalanced(Sum(e1, e2)) + ")"
}


showBalanced(Sum(ProdOfExpr(Number(2), Var("x")), Var("y")))

showBalanced(ProdOfExpr(Sum(Number(2), Var("x")), Var("y")))
