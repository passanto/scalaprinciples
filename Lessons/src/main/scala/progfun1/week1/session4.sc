/*
Lecture 1/7


Greatest Common Divisor
and
Factorial


IF a function calls itself (recursion) as its LAST step, the function's stack
frame can be reused. (tail recursion)
Current execution's variables are not required by the next step (recursion),
hence they can be replace and the same stack can be used (by the next recursive
step)

*/

import scala.annotation.tailrec

// last call is recursive
@tailrec
def gcd(a: Int, b: Int): Int = {
  if (b == 0) a else gcd(b, a % b)
}
gcd(20, 505)
gcd(7, 21)


// last call is a multiplication
def notOptimalFactorial(n: Int): Int = {
  if (n == 0) 1
  else n * notOptimalFactorial(n - 1)
}
notOptimalFactorial(7)



def factorial(n: Int): Int = {
  // last call is recursive (loop...)
  @tailrec
  def loop(acc: Int, n: Int): Int = {
    // BASE CASE
    if (n == 0) acc
    // recursion
    else loop(acc * n, n - 1)
  }

  // returns 1 since base case is n=0
  loop(1, n)
}
factorial(4)
factorial(10)