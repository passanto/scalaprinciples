/*

Lectures 1/1-4
------------



SUBSTITUTION MODEL:
Reducing an expression to a value

Parameterized functions are evaluated:
  -arguments from left -> right
  -replaces function application with right side AND
    replaces formal parameters with actual arguments
ie:

(call by value - default used by Scala)
evaluates function arguments only once:
---------------------------------------
sumOfSquare(3, 2+2) =>
sumOfSquare(3, 4) =>
square(3) + square(4) =>
3*3 + square(4) =>
9 + square(4) =>
9 + 4*4 =>
9 + 16 =>
25

(call by name - if the parameter has => before)
function argument not evaluated if the parameter is not used in the body:
-------------------------------------------------------------------------
sumOfSquare(3, 2+2) =>
square(3) + square(2+2) =>
3*3 + square(2+2) =>
9 + square(2+2) =>
9 + (2+2) * (2+2) =>
9 + 4 * (2+2) =>
9 + 4 * 4 =>
25
 */

// def
// is a definition by name.
// it is evaluated only (and every time) it is used
def square(x: Double): Double = x * x

square(4)
square(square(4))
square(4 + 3)
square(4) + square(3)

def sumOfSquare(x: Double, y: Double): Double = square(x) + square(y)

sumOfSquare(4, 3)
sumOfSquare(3, 2 + 2)

// val
// is a definition by value.
// it is evaluated immediately
val sOS = sumOfSquare(3, 7)



// write AND and OR without && and ||

def and(x: Boolean, y: => Boolean): Boolean = {
  if (x) y
  else x
}

def or(x: => Boolean, y: => Boolean): Boolean = {
  if (!x) y
  else x
}

def loop: Boolean = loop

and(true, true)
and(true, false)
and(false, true)
and(false, false)

and(false, loop)

or(true, true)
or(true, false)
or(false, true)
or(false, false)
