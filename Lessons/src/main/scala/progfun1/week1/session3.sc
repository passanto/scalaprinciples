/*
Lecture 1/6

 */

val x = 0

def f(y: Int): Int = y + 1

val res = {
  val x = f(3) // shadows outer 'x'
  x * x // return
} + x // x = 0, the outer one


/*
 Semicolons and infix operators:

 // these get interpreted as two different lines
 someLongExpression
 + someOtherExpression

 solutions are:

  (someLongExpression
    + someOtherExpression)

  or

  someLongExpression +
    someOtherExpression

  */
