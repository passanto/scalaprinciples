/*
Lecture 1/5


NEWTON method:


SQR of x=2:
initial estimate = 1

Estimation:    Quotient:                Mean:
(Mean(i-1))    (x / E)                  ((Q + E) / 2)
-----------------------------------------------------
1              2 / 1 = 2                1.5
1.5            2 / 1.5 = 1.333          1.4167
1.4167         2 / 1.4167 = 1.4118      1.4142
1.4142         2 / 1.4142 = 1.414227125 1.414213562
 */


import scala.annotation.tailrec

1 + 2

def abs(x: Double) = if (x < 0) -x else x
abs(-1)



var precision: Double = 0.000000000000001
// precision = 0.001
var estimation: Double = 1
def sqrt(x: Double): Double = {

  @tailrec
  def sqrIter(estimation: Double): Double =
    if (isGoodEnoughForNotBigOrSmallNumbers(estimation)) estimation
    else sqrIter(improve(estimation))

  def isGoodEnoughForNotBigOrSmallNumbers(guess: Double) =
    math.abs(guess * guess - x) < precision

  // def isGoodEnough(estimation: Double, x: Double) =
  //   abs(estimation * estimation - x) / x < 0.000000000000001

  def improve(estimation: Double) = {
    def r = ((x / estimation) + estimation) / 2
    // println(r)
    r
  }

  sqrIter(estimation)
}

sqrt(2)
sqrt(4)
sqrt(9)
sqrt(1e-6)
//sqrt(0.000001)
sqrt(100)