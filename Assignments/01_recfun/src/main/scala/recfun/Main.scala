package recfun

import scala.annotation.tailrec

object Main {

  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int = {
    if (c < 0 || r < 0 || c > r) throw new IllegalArgumentException("Col and row must be 0 or greater. Col length must be lower than row length")
    else if (c == 0 || c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }


  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {
    val opens = 0

    def valueOfChar(c: Char): Int = c match {
      case '(' => 1
      case ')' => -1
      case _ => 0
    }

    @tailrec
    def balanceIter(chars: List[Char], counter: Int): Int = {
      val open: Int = counter + valueOfChar(chars.head)

      if (open < 0 || chars.tail.isEmpty) open
      else balanceIter(chars.tail, open)
    }

    if (chars.isEmpty) true
    else balanceIter(chars, opens) == 0
  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {

    def countChangeIter(money: Int, coins: List[Int]): Int = {
      if (money == 0) 1
      else if (money < 0) 0
      else if (coins.isEmpty) 0
      else countChangeIter(money - coins.head, coins) + countChangeIter(money, coins.tail)
    }

    if (money == 0 || coins.isEmpty) 0
    else countChangeIter(money, coins.sortBy(-_))
  }

}
