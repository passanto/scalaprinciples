package funsets

object Main extends App {

  import FunSets._

  println(contains(singletonSet(1), 1))

  val s1: Set = singletonSet(1)
  val s2: Set = singletonSet(2)
  val s3: Set = singletonSet(3)
  val s4: Set = singletonSet(4)
  printSet(map(union(s1, union(s2, union(s3, s4))), x => x * x))

  // a set that contains all numbers between 1 and 100
  val s1_to_100: Set = (x: Int) => x > 0 && x < 101
  contains(s1_to_100, 0)
  contains(s1_to_100, 1)
  contains(s1_to_100, 100)
  contains(s1_to_100, 101)


}
